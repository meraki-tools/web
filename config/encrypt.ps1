function writeCredential {
    [CmdletBinding()]
    param(
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$Username,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$Password,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$Driver,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$DbServer,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$DbName
    )

    [pscustomobject]@{
        Driver = $Driver
        DBServer = $DbServer
        DB = $DbName
        UserName = $Username
        Password = $Password
    } | Export-Csv -Path "save/encrypted/file/here"  
}

$driver = "DRIVER={ODBC Driver 17 for SQL Server}"
$dbServer = "SERVER=placeholder\placeholder,portNumber"
$db = "DATABASE=placeholder"
$credential = Get-Credential
$credential.Password | ConvertFrom-SecureString -outvariable pw
write-Output $pw
$userName = $credential.UserName

writeCredential -Username $userName -Password $pw -Driver $driver -DbServer $dbServer -DbName $db