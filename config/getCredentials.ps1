$ImportPath = "save/encrypted/file/here"
$ImportDBCredentials = Import-Csv -Path $ImportPath -Delimiter ','
$SecurePasswordString = $ImportDBCredentials.Password | ConvertTo-SecureString
$UnsecurePassword = (New-Object PSCredential "user", $SecurePasswordString).GetNetworkCredential().Password
$Output = "" + $ImportDBCredentials.Driver + "; " + $ImportDBCredentials.DBServer + "; " + $ImportDBCredentials.DB + "; UID=" + $ImportDBCredentials.UserName + "; PWD=" + $UnsecurePassword
Write-Output $Output