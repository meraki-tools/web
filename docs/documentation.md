---
title: "Dokumentation"
subtitle: "Anbindung Meraki Dashboard API an Frontend"
author: "Stefan Dortmund"
date: \today
keywords: [Markdown, Documentation]
lang: "de"
titlepage: true
titlepage-color: "008d58"
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
toc: true
toc-own-page: true
book: true
classoption: oneside
code-block-font-size: \scriptsize
...

# 1. Einleitung

In `meraki tools` befinden sich die beiden Projekte `web` sowie `desktop`, die verschiedene Funktionalitäten der Meraki API für Benutzer remote über eine entsprechende Applikation sowie lokal als Python Skript zur Verfügung stellen und auf [GitLab](https://gitlab.com/meraki-tools) zu finden sind. Die in dieser Dokumentation beschriebenen Informationen gelten jeweils für die Anbindung an ein Frontend. Weitere Details für die lokale Nutzung der Skripte sind der entsprechenden Dokumentation zu entnehmen. 

![Projekte im Git-Repository](img/22.PNG)

![Verzeichnisstruktur im Projekt 'web'](img/23.PNG)

\pagebreak

Die folgende Skizze zeigt ein Ablaufdiagramm beim Aufruf durch das Frontend.

![Ablaufdiagramm Frontend](img/21.PNG)

# 2. Meraki Dashboard API

Die Dashboard API bietet eine Schnittstelle für die Meraki Plattform an.

## 2.1 Dokumentation

Die Dokumentation befinden sich unter [https://documentation.meraki.com](https://documentation.meraki.com/zGeneral_Administration/Other_Topics/The_Cisco_Meraki_Dashboard_API).

## 2.2 Zugriff auf API

Um Zugriff auf die API zu erhalten, muss dieser erst für eine Organisation erlaubt werden.

Dazu im Dashboard unter `Organization` &rarr; `Settings` &rarr; `Dashboard API access` den Hacken setzen.

![Zugriff auf API](img/1.PNG)

## 2.3 API Key erzeugen

Im Dashboard unter `My profile` &rarr; `API access` &rarr; `Generate new API key` einen neuen Key erzeugen.

![API Key erzeugen](img/2.PNG)

**Hinweis:**

Der API Key dient zur **Authentifizierung**. Sollten Unbefugte Zugriff auf diesen erhalten haben, so muss der Key **unverzüglich** im Dashboard unter `My profile` &rarr; `API access` &rarr; `Revoke` widerrufen werden!

# 3. Meraki Dashboard API Python

Das GitHub Repository befindet sich unter https://github.com/meraki/dashboard-api-python.

![Python Modul](img/5.PNG)

\pagebreak

Aktuell gibt es zwei Version des `meraki` Moduls:

Modul | Beschreibung
| -- | -------- |
meraki | aktuelle Version des Moduls
meraki_v0 | veraltete Version, welche noch aus Kompatibilitätsgründen besteht

In `meraki` befinden sich die Ordner `aio` und `api`, welche unterschiedliche Funktionalitäten bieten.

![Python Modul](img/19.PNG)

Typ | Beschreibung
| - | ------- |
| aio | Funktionen, die mehrere Abfrage an die Meraki API gleichzeitig senden
| api | Funktionen, die normale Abfrage (nacheinander) an die Meraki API senden

Der Releasezyklus beträgt meistens **1 bis 2 Wochen**.

Der Changelog befinden sich unter https://developer.cisco.com/meraki/whats-new/#!overview.

\pagebreak

## 3.1 Benutzung im Skript

Um das Modul `meraki` nutzen zu können, muss dieses zunächst importiert werden. Dazu kann entweder das komplette Modul importiert werden oder nur bestimmte Funktionalitäten (`aio` oder `api`).

```python
import meraki.api
```

Danach kann ein Objekt vom Typ `DashboardAPI` (bzw. `AsyncDashboardAPI` bei `aio`) erzeugt werden.

```python
apimeraki = meraki.DashboardAPI(
    api_key='apiKey',
    output_log=False,
    print_console=False,
    suppress_logging=True,
    requests_proxy='proxy'
)
```

Bei der Erzeugung eines neuen Objektes können mehrere Parameter mit angegeben werden:

Parameter | Bedeutung
| -- | -------- |
api_key | enhält Meraki API Key des Benutzers, **wird immer benötigt!**
output_log | `False`, da mithilfe von `exceptions.py` eine eigene Logdatei erstellt wird
print_console | gibt Fehlermeldungen auf Konsole aus. `False`, da `output_log` auch `False`
suppress_logging | verhindert explizit die Ausgabe von Fehlermeldungen in der Konsole
requests_proxy | Angabe eines Proxy Servers der für die Abfrage verwendet werden soll

Eine Abfrage erfolgt durch Aufruf der passenden Funktion des Objektes `apimeraki`.

```python
organizations = apimeraki.organizations.getOrganizations()
```

Als Rückgabeparameter erhält Python **immer** eine Liste bestehend aus Dictionaries (JSON).

![Ausgabe getOrganizations()](img/4.PNG)

\pagebreak

Werden bei Anfragen Übergabeparameter benötigt, so gibt es dabei zwei Möglichkeiten: 

- `required arguments`, die **immer** angegeben werden müssen

![Abfrage getOrganizationNetworks()](img/12.PNG)

- `optional arguments`, die bei Bedarf angegeben werden können

![Abfrage getOrganizationNetworks() mit optinalem Argument](img/13.PNG)

## 3.2 Benutzung über Konsole

Neben der Ausführung in Skripten kann Python Code auch direkt in einer Konsole verwendet werden. Dies ist manchmal nützlich, um bestimmte Funktionalitäten testen zu können, ohne das komplette Skript ausführen zu müssen. Um in diesen sogenannten `interactive mode` zu kommt, wird in einer Shell der Befehl `python` eingetippt. Sollen Module aus einer virtuellen Umgebung (z.B. `meraki`) benötigt werden, so muss diese zunächst aktiviert werden.

```console
(meraki-v1) C:\PythonScripts\library > python
```

Danach kann beispielsweise die Funktion `alter` vom Skript `database.py` aufgerufen werden. 

![Ausführung von alter()](img/15.PNG)

\pagebreak

## 3.3 Aktualisierung

Da es bei einem neuen Release passieren kann, dass die Entwickler etwas an einer Funktion oder an einem Übergabeparameter ändern, sollte das Modul eigentlich nicht aktualisiert werden. In manchen Situation kann es aber dennoch sinnvoll sein, dies zu tun:

- Implementierung neuer Funktionalitäten, die in aktueller Version nicht vorhanden sind

In diesem Beispiel wird die Version **1.0** auf Version **1.5** aktualisiert.

In dem Ordner `PythonScripts` wird eine neue virtuelle Umgebung erzeugt und diese aktiviert:

```console
C:\PythonScripts> python -m venv meraki-v15
C:\PythonScripts> .\meraki-v15\Scripts\activate.bat
```

Nun werden die benötigten Module aus `requirements.txt` installieren:

```console
(meraki-v15) C:\PythonScripts> pip install -r requirements.txt
```

Falls gewünscht, können die anderen Module ebenfalls aktualisiert werden:

```console
(meraki-v15) C:\PythonScripts> python update.py
```

Danach wird das `meraki` Modul mit Angabe der gewünschten Version installiert:

```console
(meraki-v15) C:\PythonScripts> pip install meraki
```

Im Ordner `batch` müssen die Shellskripts angepasst werden, dazu den Ordner am besten kopieren:
```yaml
# changePort.cmd
- C:\PythonScripts\meraki-v1\Scripts\activate.bat ...
+ C:\PythonScripts\meraki-v15\Scripts\activate.bat ...
```

Zum Schluss sollten die Funktionalitäten der neuen Version ausführlich getestet werden.

**Hinweis:**

Die beschriebenen Schritte beeinträchtigt **nicht** die bestehende Implementierung, da diese weiterhin in einer eigenen virtuellen Umgebung (in diesem Fall `meraki-v1`) läuft.

# 4. Konfiguration

Auf dem Windows Server muss der ODBC Driver 17 for SQL sowie der Python Interpreter in der Version 3.7 vorhanden sein. Danach kann mithilfe des Python Package Installers (`PIP`) das Modul `virtualenv` installiert und eine neue virtuelle Umgebung erzeugt werden. Dazu muss in den Ordner `PythonScripts` gewechselt und die folgenden Befehle in einer Shell ausgeführt werden:

```console
# installiert das Paket virtualenv
C:\PythonScripts> python -m pip install --user virtualenv
# erzeugt eine neue virtuelle Umgebung
C:\PythonScripts> python -m venv meraki-v1
```

**Hinweis:**

Der Name der virtuellen Umgebung setzt sich in diesem Fall aus dem Modulnamen `meraki` sowie der verwendeten Versionsnummer `v1.0` zusammen. 

Danach kann die virtuelle Umgebung `meraki-v1` aktiviert und das Meraki Modul installiert werden.

```console
# virtuelle Umgebung aktivieren
C:\PythonScripts> .\meraki-v1\Scripts\activate.bat
# Meraki Modul installieren
(meraki-v1) C:\PythonScripts> pip install meraki
```

In der Textdatei `requirements.txt` befindet sich eine Auflistung von Modulen, die ebenfalls installiert werden müssen. Dazu wird der folgende Befehle in einer Shell ausgeführt:

```console
(meraki-v1) C:\PythonScripts> pip install -r requirements.txt
```

Danach kann das Repository von GitLab heruntergeladen und im Ordner `PythonScripts` entpackt werden. Hier müssen dann noch einige Anpassungen an den Konfigurationsdateien gemacht werden:

Im Ordner `batch` muss jeweils noch der Pfad zum Python Skript angepasst werden.

```yaml
- path\to\virtual\environment\...\activate.bat && python path\to\initiate.py
+ C:\PythonScripts\meraki-v1 \...\activate.bat && python C:\PythonScripts\...\initiate.py
```

Im Ordner `config` müssen die beiden Dateien `key` sowie `proxy` erstellt und die entsprechenden Informationen dort eingetragen werden. Wird kein Proxy Server verwendet, so muss die Datei dennoch erstellt aber dort nichts eingetragen werden. Danach müssen die beiden Powershell Skripte `encrypt` sowie `getCredentials` noch angepasst werden.

```Powershell
# encrypt.ps1
- } | Export-Csv -Path "save/encrypted/file/here"
+ } | Export-Csv -Path "C:\PythonScripts\config\dbCredEnc" 

- $dbServer = "SERVER=placeholder\placeholder,portNumber"
+ $dbServer = "SERVER=placeholder\placeholder,portNumber"

- $db = "DATABASE=placeholder"
+ $db = "DATABASE=placeholder"

# getCredentials.ps1
- $ImportPath = "save/encrypted/file/here"
+ $ImportPath = "C:\PythonScripts\config\dbCredEnc"
```

Mithilfe von `encrypt` wird die verschlüsselte Datei `dbCredEnc` erstellt, welche die Anmeldedaten für die Microsoft SQL Datenbank enthält. Nach dem Start des Skripts öffnet sich eine Maske, in die der Benutzername sowie das Passwort eingetragen wird.

![Ausführung von encrypt.ps1](img/20.PNG)

In der Datei `logging.ini` befinden sich verschiedene Parameter, welches für die Erstellung der Logdatei benötigt werden. In Zeile 18 muss der Ablageort dieser Datei angepasst werden:

```yaml
- args=('/path/to/error.log',)
+ args=('C:\PythonScripts\error.log',)
```

In dem Skript `exceptions` muss dieser Parameter in der Zeile 15 ebenfalls angepasst werden:

```python
- logging.config.fileConfig('/path/to/logging.ini', disable_existing_logger=False)
+ logging.config.fileConfig('C:\PythonScripts\logging.ini', disable_existing_logger=False)
```

In dem Skript `functions` müssen ebenfalls noch Parameter einiger Funktionen angepasst werden:

```python
def dbCredentialsPs():
    - p = Popen(["Powershell.exe", "path/to/script.ps1"] ...
    + p = Popen(["Powershell.exe", "C:\\PythonScripts\\config\\getCredentials.ps1"] ...
```

```python
def apiCredentials():
    - with open('path/to/key.txt', 'r') as f:
    + with open('C:\PythonScripts\config\key.txt', 'r') as f:
```

```python
def proxySettings():
    - with open('path/to/proxy.txt', 'r') as f:
    + with open('C:\PythonScripts\config\proxy.txt', 'r') as f:
```

Danach können die Datensätze mithilfe des Skripts `initiate.cmd` in der Datenbank erstellt werden:

```console
(meraki-v1) C:\PythonScripts> initiate.cmd
```

Sollte es zu Problemen bei der Ausführung kommen, so wird ein neuer Eintrag in die Logdatei geschrieben. Eine detaillierte Erklärung möglicher Fehler findet sich im Kapitel [Fehlerbehandlung](#fehlerbehandlung).

Im Frontend wird nun noch eine Task erstellt, die zweimal täglich um 08:00 Uhr und 12:00 Uhr das Skript `initiate.cmd` ausführt und damit die Datensätze in der Datenbank aktuell hält.

## 4.1 Verzeichnisstruktur

Nachdem die Konfiguration abgeschlossen wurde, finden sich im Ordner die folgende Struktur.

In `batch` befinden sich die `Connectors`, welche als Verbindung zwischen Front- und Backend dienen. Die einzelnen Skripte sind Batchdateien, welche ein Python Skript in einer virtuellen Umgebung mit optionalen Parametern ausführen und werden über das Frontend aufgerufen. Eine detaillierte Beschreibung des Konzepts befindet sich im Kapitel [Connectors](#connectors).

```yaml
|-- batch/
    |-- changePort.cmd
    |-- groupPolicy.cmd
    |-- initiate.cmd
    |-- macLookup.cmd
```

In `config` befinden sich die Konfigurationsdateien für den Meraki API Key, die Microsoft SQL Datenbank sowie für einen Proxy Server. Die Anmeldedaten für die Datenbank werden dabei verschlüsselt mithilfe des Skripts `encrypt` im Ordner abgelegt und können vom Frontend über das Skript `getCredentials` wieder gelesen werden. 

```yaml
|-- config/
    |-- dbCredEnc           # Datei für den Datenbankzugriff
    |-- encrypt.ps1         # verschlüsselt Anmeldedaten für Datenbank
    |-- getCredentials.ps1  # entschlüsselt Anmeldedaten für Datenbank
    |-- key.txt             # für den Zugriff auf Meraki API
    |-- proxy.txt     
```

\pagebreak

In `docs` befindet sich die Dokumentation des Projekts als Markdown sowie als PDF Datei.

```yaml
|-- docs/
    |-- img/
        |-- ...
    |-- documentation.md
    |-- documentation.pdf
```

In `library` befinden sich die Skripte, die vom Front- bzw. Backend aufgerufen werden.

```yaml
|-- library/
    |-- changePort.py       # gibt Port-Einstellungen aus oder ändert diese
    |-- database.py         # für die Interaktion mit der Microsoft SQL Datenbank
    |-- exceptions.py       # Exception Handling über einen Python Dekorator
    |-- functions.py        # Sammlung verschiedener Hilfsfunktionen
    |-- groupPolicy.py      # gibt Group Policies eines Clients aus oder ändert diese
    |-- initiate.py         # aktualisert die Datenbank, läuft zweimal täglich
    |-- macLookup.py        # liefert zu einer MAC-Adresse Information von der Meraki API
    |-- updateTags.py       # Skript, welches die Tabelle meraki_portTags aktualisert
```

In `meraki-v1` befinden sich die Python Module der zuvor erstellten virtuellen Umgebung.

```yaml
|-- meraki-v1/              # Ordner für die Python Virtual Environment
    |-- ...
```

Im Ordner `PythonScripts` befinden sich darüber hinaus noch weitere Dateien.

```yaml
|-- error.log               # wird beim ersten Ausführen von initiate.py erzeugt
|-- logging.ini             # Konfigurationsdatei für die Logdatei
|-- requirements.txt        # Liste mit Module, die für das Backend benötigt werden
```

# 5. Funktionsweise

## 5.1 Aufbau der Skripte

In der ersten Zeile wird die verwendete Python Version definiert.

```python
#!/usr/bin/env python3
```

\pagebreak

Danach erfolgen verschiedene Angaben zum Ersteller bzw. zur Version.

```python
__author__  = 'Autor'
__date__    = 'Datum'
__license__ = 'GNU GPLv3'
__version__ = '1.0'
```

Beim Import der Module wird zwischen drei verschiedenen Arten unterschieden.

```python
# Standard Module
import asyncio

# externe Module aus PIP 
import meraki

# eigene Funktionen
from exceptions import exceptDeco
```

Optional werden globale Variablen angelegt, welche von jeder Funktion genutzt werden können.

```python
apiOrgDict = dict()
...
```

Danach erfolgt die Definiton der einzelnen Funktionen bzw. Klassen.

```python
def initiateApi():
    ...
```

Zum Schluss wird die Ausführungsreihenfolge der einzelnen Funktionen definiert.

```python
if __name__ == '__main__':
    initiateApi()
    asyncio.run(initiateAio())
```

## 5.2 Speicherung der Daten

Für die Speicherung der von der Meraki API zurückgelieferten Datensätze wird ein `Dictionary` verwendet. Dieser Datentyp besteht aus einem `Key-Value-Pair`, wobei der `Key` immer eindeutig sein muss. Somit werden doppelte Datensätze verhindert.

## 5.3 Skripte für Backend

Das Skripte `initiate` wird über eine Batchdatei (zu finden in `batch`) vom Frontend aufgerufen.

### 5.3.1 Anbindung an die Datenbank

Die Klasse `Database` bietet verschiedene Funktionen, um mit der Datenbank interagieren zu können.

In der Funktion `__init__` wird für jedes neu erstellte Objekt eine Verbindung zur Datenbank aufgebaut sowie ein `Cursor` erzeugt, mit dem SQL-Anfragen geschickt werden können. Mit dem Schlüsselwort `self` wird dabei auf die Instanz des übergebenen Objekts referenziert.

```python
def __init__(self):
    self.conn = pyodbc.connect(dbCredentials())
    self.curs = self.conn.cursor()
```

In der Funktion `create` werden die einzelnen Tabellen für die Datenbank erzeugt. Dafür wird zunächst in einer `if`-Abfrage mithilfe des `Cursors` überprüft, ob die entsprechende Tabelle bereits in der Datenbank existiert. Falls nicht, wird diese mithilfe eines SQL-Statements angelegt.

```python
def create(self):
    if not self.curs.tables(table='meraki_access', tableType='TABLE').fetchone():
        self.curs.execute(
            'CREATE TABLE {tableName} '
            '('
            'macAddress varchar(255) PRIMARY KEY, '
            'networkId varchar(255), '
            'serialNumber varchar(255), '
            'name varchar(255), '
            'organizationName varchar(255), '
            'networkName varchar(255)'
            ')'
            .format(tableName='meraki_access')
        )
...
```

In der Funktion `insert` werden die Informationen von der Meraki API in die entsprechende Tabelle geschrieben. Als Übergabeparameter erhält die Funktion den Namen einer Tabellen sowie eine Liste bestehend aus Dictionaries. Für das Einfügen wird das SQL-Statement `merge` benutzt.

```python
def insert(self, tableName, values):
    if tableName == 'meraki_access':
        self.curs.execute(
            (
                'MERGE INTO {tableName} as TARGET '
                'USING (SELECT * FROM  (VALUES {value}) '
                'AS s (
                    {macAddress}, {networkId}, {serialNumber},
                    {name}, {organizationName}, {networkName}) '
                ') AS SOURCE '
                'ON TARGET.{macAddress}=SOURCE.{macAddress} '
                'WHEN MATCHED AND TARGET.{networkId}<>SOURCE.{networkId} 
                    OR TARGET.{name}<>SOURCE.{name} 
                    OR TARGET.{organizationName}<>SOURCE.{organizationName} 
                    OR TARGET.{networkName}<>SOURCE.{networkName} THEN '
                'UPDATE SET 
                    {networkId}=SOURCE.{networkId}, 
                    {serialNumber}=SOURCE.{serialNumber}, 
                    {name}=SOURCE.{name}, 
                    {organizationName}=SOURCE.{organizationName}, 
                    {networkName}=SOURCE.{networkName} '
                'WHEN NOT MATCHED BY SOURCE THEN '
                'DELETE '
                'WHEN NOT MATCHED BY TARGET THEN '
                'INSERT (
                    {macAddress}, {networkId}, {serialNumber}, 
                    {name}, {organizationName}, {networkName} '
                ' VALUES (
                    SOURCE.{macAddress}, SOURCE.{networkId}, SOURCE.{serialNumber}, 
                    SOURCE.{name}, SOURCE.{organizationName}, SOURCE.{networkName}
                );'
                .format(
                    tableName=tableName,
                    value=','.join([str(i) for i in values]),
                    macAddress='macAddress',
                    networkId='networkId',
                    serialNumber='serialNumber',
                    name='name',
                    organizationName='organizationName',
                    networkName='networkName'
                )
            )
        )
...
```

Zeile | Erklärung
| - | ------- |
| 5 | Definiert die als Parameter übergebene Tabelle `tableName` als `TARGET`
| 6-10 | Erstellt eine temporäre Tabelle mit den Werten aus `values` als `SOURCE`
| 11 | Verbindet die beiden Tabellen jeweils mithilfe des Primärschlüssels `macAddress`
| 12-15 | Matched, falls die Spalte in `TARGET` vorhanden ist und sich ein Wert geändert hat
| 16-21 | Falls `true` werden die Datensätze einer Spalte (ohne Primärschlüssel) aktualisiert
| 22-23 | Falls Werte in `TARGET` vorhanden sind, nicht aber in `SOURCE` werden diese gelöscht$^1$
| 24 | Matched, falls die Spalte in `SOURCE` vorhanden ist, nicht aber in `TARGET`
| 25-31 | Falls `true` werden alle Datensätze einer Spalte der Tabelle `TARGET` hinzugefügt
| 32-40 | Placeholder im SQL-Statement werden den passenden Spaltennamen zugeordnet
| 34 | Die als Parameter übergebene Listen in `values` werden in einen String gespeichert$^2$

$^1$ z.B. der Fall wenn ein defekter Switch ausgetauscht wurde, dieser aber noch in der Tabelle steht

$^2$ notwendig, da mithilfe von `merge` keine Dictionaries sondern nur Strings eingefügt werden können

\pagebreak

In der Funktion `select` wird ein Datensatz zusammen mit dem Namen der Tabelle zurückgegeben.

```python
def select(self, tableNames, column, condition, value):
    for tableName in tableNames:
        result = self.curs.execute(
            'SELECT {column} FROM {tableName} WHERE {condition} = ?'
            .format(
                tableName=tableName, column=column, condition=condition
                ), value)
            .fetchone()
        if result is not None:
            return tableName, result
```

Zeile | Erklärung
| - | ------- |
| 1 | Übergabeparameter: Liste mit Tabellen$^1$, Spalten, Bedingung und gesuchter Wert
| 2 | In einer `for`-Schleife werden die einzelnen Tabellen der Liste durchgegangen
| 3-4 | Mithilfe von `select` wird nach `value` gesucht und das Ergebnis in `result` gesichert
| 5-7 | Placeholder im SQL-Statement werden den passenden Spaltennamen zugeordnet$^2$
| 8 | Liefert ein Tuple und gibt `none` zurück, falls kein Datensatz gefunden wurde
| 9-10 | Falls `result != None`, wird Datensatz zusammen mit Tabellenname zurückgegeben

$^1$ Da nicht genau gesagt werden kann, in welcher Tabelle sich der Wert befindet

$^2$ Fragezeichen (Zeile 4) entspricht Wert aus `value` und kann nicht als Placeholder übergeben werden

Die Funktion `alter` fügt einer Tabelle eine neue Spalte hinzu.

```python
def alter(self, tableName, columnName):
    self.curs.execute(
        'ALTER TABLE tableName ADD columnName varchar(255) NOT NULL DEFAULT(1)'
        .format(
            tableName=tableName, columnName=columnName
        )
    )
```

Die Funktion `__enter__` wird benötigt, wenn eine Klasse mithilfe eines `Context Managers` genutzt wird.

```python
def __enter__(self):
    return self
```

Die Funktion `__exit__` wird zum Schluss aufgerufen und stellt sicher, dass alle Änderungen an die Datenbank übermittelt wurden und schließt danach die Verbindung wieder. Diese Funktion stellt somit die Integrität der Datenbank zu jedem Zeitpunkt sicher.

```python
def __exit__(self, exc_type, exc_value, traceback):
    self.conn.commit()
    self.conn.close()
```

### 5.3.2 Aktualisierung der Datensätze

Das Skript `initiate` aktualisiert die Datensätze in der Microsoft SQL Datenbank. Die Abfragen an die Meraki API wurden dabei in den beiden Funktionen `initiateApi` sowie `initiateAio` aufgeteilt. Grund hierfür war, dass die Abfrage der Admins bzw. der API Requests einer Organisation nicht mithilfe von `meraki.aio` geschickt werden konnten, da diese dort eine `Exception` werfen, die das Skript beendet.

Für jede Tabelle wird zunächst ein eigenes Dictionary erstellt.

```python
apiOrgDict = dict()
accessDict = dict()
...
```

In der Funktion `initiateApi` werden die Admins und API Requests einer Organisation abgefragt.

```python
def initiateApi():
    ...
    organizations = apimeraki.organizations.getOrganizations()
    for organization in organizations:
        ...
```

In einem `try-except`-Block werden die einzelnen Organisationen durchlaufen und die benötigten Informationen (Zeitraum: letzte 3 Tage) abgefragt. Das zusätzliche Exception Handling ist in diesem Fall notwendig, da bei beiden Abfragen Fehler auftreten können, welche nicht von `@exceptDeco` behandelt werden können. Aus diesem Grund werden die Meldungen auch einfach auf der Konsole ausgegeben und nicht weiter behandelt.

```python
try:
    admins = apimeraki.organizations.getOrganizationAdmins(organization['id'])
    requests = apimeraki.organizations.getOrganizationApiRequests(
        organization['id'], timespan=60*60*24*3, perPage=1000, total_pages='all')
except meraki.exceptions.APIError as e:
    print(e)
```

\pagebreak

Danach werden die zurückgelieferten Informationen in das entsprechende Dictionary gespeichert.

```python
 for admin in admins:
    apiAdmDict.update({admin['id'] : (
        admin['name'], admin['email'], admin['id'], organization['name'])}
    )
for request in requests:
    apiReqDict.update({request['ts'] : (
        request['adminId'], getAdminName(admins, request['adminId']), 
        request['method'], mergeApiRequest(request['path'], 
        request['queryString']), getHttpMessage(request['responseCode']), 
        convertTimestamp(request['ts']), organization['name'])}
    )
```

In der Funktion `initiateAio` werden weitere Informationen einer Organisation abgefragt.

```python
async def initiateAio():
    ...
    organizations = await aiomeraki.organizations.getOrganizations()
    ...
```

Für jede Organisation wird in `organizationTasks` ein eigenes `future`-Objekt erzeugt. In dieses werden die zukünftigen Ergebnisse der API Abfrage gespeichert. Dadurch ist es möglich, das mehrere Anfragen an Meraki gleichzeitig gestellt werden und somit die Bearbeitungszeit deutlich reduziert werden kann.

```python
organizationTasks = [
    listOrganizations(aiomeraki, organization) for organization in organizations]
    for task in asyncio.as_completed(organizationTasks):
        await task
```

Die einzelnen Tasks rufen zuerst die Funktion `listOrganizations` auf. Als Übergabeparameter wird das in `initiateAio` erzeugte `AsyncDashboardAPI`-Objekt (Zeile 61-66) sowie die Organisation übergeben.

```python
async def listOrganizations(aiomeraki: meraki.aio.AsyncDashboardAPI, organization):
    apiOrgDict.update({("('"+organization['name']+"')") : ''})
    networks = await aiomeraki.organizations.getOrganizationNetworks(organization['id'])
    networkTasks = [listNetworks(aiomeraki, organization, network) for network in network]
    for task in asyncio.as_completed(networkTasks):
        await task
    return None
```

Zeile | Erklärung
| - | ------- |
| 2 | Name einer Organisation wird als Key in `apiOrgDict` gespeichert$^1$
| 3 | Abfrage der Netzwerke innerhalb einer Organisation
| 4 | Für jedes Netzwerk wird ein `future`-Objekt erzeugt und in `networkTasks` gespeichert
| 5-6 | Einzelne Tasks werden ausgeführt und rufen Funktion `listNetworks` auf 
| 7 | Rückgabe von `None`, da sonst eine `Exception` geworfen wird

$^1$ Da `insert` als Parameter eine Liste erwartet, muss der Wert noch entsprechend angepasst werden

In der Funktion `listNetworks` werden die Clients, Devices, Group Policies sowie die Switchport Tags eines Netzwerkes abgefragt. Als Übergabeparameter wird das in `initiateAio` erzeugte `AsyncDashboardAPI`-Objekt (Zeile 61-66), die Organisation sowie das Netzwerk übergeben.

```python
async def listNetworks(aiomeraki: meraki.aio.AsyncDashboardAPI, organization, network):
    clients = await aiomeraki.networks.getNetworkClients(
        network['id'], timespan=60*60*24*3, perPage=1000, total_pages='all')
    devices = await aiomeraki.networks.getNetworkDevices(network['id'])
    policies = await aiomeraki.networks.getNetworkGroupPolicies(network['id'])
    for client in clients:
        clientDict.update({client['mac'] : (client['mac'], network['id'], client['id'], 
        convertNoneToNull(client['description']), organization['name'], network['name'])})
    for device in devices:
        if device['firmware'].split('-')[0] == 'switch':
            switchDict.update({device['mac'] : (
                device['mac'], network['id'], device['serial'], device['name'],
                organization['name'], network['name'], getCityFromAdress(device['adress'])
            swPort = await aiomeraki.switch.getDeviceSwitchPorts(device['serial'])
            for port in swPort:
                apiTagSet.update(port['tags'])
                swPortDict.update({(device['mac'], int(port['portId'])) : ''})
    for tags in apiTagSet:
        apiTagDict.update({(organization['name'], network['name'], tags) : ''})
    for policy in policies:
        policyDict.update({(network['id'], 
        network['name'], policy['groupPolicyId'], policy['name']) : ''})
    return None
```

Zeile | Erklärung
| - | ------- |
| 2-3 | Clients, die in den letzten 3 Tagen aktiv waren werden abgefragt
| 4 | Access Points und Switches (Devices) werden abgefragt
| 5 | Group Policies werden abgefragt
| 6-8 | Speicherung der Daten in `clientDict`, MAC-Adresse dient als Key$^1$
| 11-14 | Speicherung der Daten in `switchDict`, MAC-Adresse dient als Key$^2$
| 15 | Ports eines Switches werden abgefragt
| 17 | Tags aus `port` werden in `Set` gespeichert, um Duplikate zu verhindern
| 18 | Speicherung der Daten in `swPortDict`, MAC-Adresse dient als Key
| 20 | Speicherung der Daten in `apiTagDict`, Organisation und Netzwerk dienen als Key
| 22-23 | Speicherung der Daten in `policyDict`, alle Werte dienen hier als Key
| 24 | Rückgabe von `None`, da sonst eine `Exception` geworfen wird

$^1$ Meraki liefert `None`-Werte zurück, die Datenbank akzeptiert aber nur `Null` als Wert

$^2$ Zur Unterscheidung von Access Points und Switchen dient der Wert aus `device['firmware']`

Durch folgende Definition wird die Ausführungsreihenfolge für das Skript definiert.

```python
if __name__ == "__main__":
    initiateApi()
    asyncio.run(initiateAio())
```

## 5.4 Skripte für Frontend

Die folgenden Skripte werden über eine Batchdatei (zu finden in `batch`) vom Frontend aufgerufen.

### 5.4.1 MAC Adresse suchen

Das Skript `macLookup` gibt Informationen zu einer übergebenen MAC-Adresse zurück.

Als Argument werden dem `Getter` **immer** zwei Parameter übergeben und als Variable gespeichert.

```python
macAdd = argv[1]
apiKey = argv[2]
```

Pos. | Variabel | Erklärung
| - | - | -------- |
1 | macAdd | Die MAC Adresse nach der in der Datenbank gesucht werden soll
2 | apiKey | Enhält Meraki API Key des Benutzers, dieser wird im Frontend hinterlegt

\pagebreak

Mithilfe eines `Context Manager` wird danach in der Datenbank nach der Variablen `macAdd` gesucht.

```python
with Database() as db:
    queryResult = db.select(
        ('meraki_access', 'meraki_client', 'meraki_switch'), 
        ('networkId, serialNumber'), 
        'macAddress', 
        macAdd
    )
```

**Rückgabewert:**

![Rückgabewert db.select()](img/8.PNG)

In einer `if`-Abfrage wird schließlich die passende Abfrage an die Meraki API geschickt.

```python
if queryResult[0] == 'meraki_access':
    print(mergeApiResponseAccess(apimeraki.devices.getDevice(queryResult[1][1])))
...
```

### 5.4.2 Port Einstellungen ändern

Das Skript `changePort` gibt Port Einstellungen von einem Switch aus oder ändert diese. 

Als Argumente werden dem `Getter` **immer** drei Parameter übergeben und als Variablen gespeichert.

```python
apiKey = argv[1]
reqTyp = argv[2]
params = list(argv[3].split(' '))
```

Pos. | Variabel | Erklärung
| - | - | -------- |
1 | apiKey | Enhält Meraki API Key des Benutzers, dieser wird im Frontend hinterlegt
2 | reqTyp | Gibt an, welche Funktion aufgerufen werden soll, `setter` oder `getter`
3 | params | `serial` und `portId` für Abfrage, in Apostroph und durch Spaces getrennt

**Rückgabewert:**

![Rückgabewert getDeviceSwitchPort()](img/6.PNG)

Beim `Setter` werden weitere Parameter übergeben, die direkt in die API Abfrage geschrieben werden.

```python
tags=list(argv[4].split(" "))
enabled=argv[5]
type=argv[6]
vlan=argv[7]
allowedVlans=argv[8].replace(' ', '')
linkNegotiation=argv[9]
```

Pos. | Variable | Erklärung
| - | -- | -------- |
4 | tags | Angabe der Tags, die einem Port zugewiesen werden sollen
5 | enabled | Aktiviert bzw. deaktiviert einen Switchport
6 | type | Definiert Typ eines Ports, möglich sind `access` oder `trunk`
7 | vlan | Bei `access` muss VLAN angegeben werden, bei `trunk` Native VLAN
8 | allowedVlans | Gilt bei Ports vom Typ `trunk`, bei `access` leerer String übergeben
9 | linkNegotiation | Angabe von Speed und Duplex Werten für einen Port

### 5.4.3 Client Group Policy ändern

Das Skript `groupPolicy` gibt die aktuelle Group Policy eines Clients aus oder ändert diese.

Als Argumente werden dem `Getter` **immer** drei Parameter übergeben und als Variablen gespeichert.

```python
apiKey = argv[1]
reqTyp = argv[2]
params = list(argv[3].split(' '))
```

Pos. | Variabel | Erklärung
| - | - | -------- |
1 | apiKey | Enhält Meraki API Key des Benutzers, dieser wird im Frontend hinterlegt
2 | reqTyp | Gibt an, welche Funktion aufgerufen werden soll, `setter` oder `getter`
3 | params | `networkId` und `clientId` für Abfrage. In Apostroph, getrennt durch Spaces 

**Rückgabewert:**

![Rückgabewert getNetworkClientPolicy()](img/7.PNG)

Beim `Setter` wird ein weiterer Parameter übergeben, der direkt in die API Abfrage geschrieben wird.

```python
groupPolicyId=argv[4])
```

Pos. | Variable | Erklärung
| - | -- | -------- |
4 | groupPolicyId | ID der Group Policy, welche auf den Client angewendet werden soll

## 5.5 Weitere Skripte

Die folgenden Skripte werden sowohl vom Front- als auch vom Backend aufgerufen.

### 5.5.1 Connectors

In `batch` befinden sich die `Connectors`, welche Funktionalitäten vom Backends dem Frontend zur Verfügung stellen. Dabei wird zunächst eine virtuelle Umgebung gestartet und danach das entsprechende Python Skript mit den passenden Übergabeparametern aufgerufen.

![Funktionsweise Connector](img/24.PNG)

```powershell
@ECHO OFF
path\to\virtual\environment\activate.bat && python path\to\macLookup.py %1 %2
```

### 5.5.2 Hilfsfunktionen

In `functions` befinden sich Funktionen, die von anderen Skripts aufgerufen werden.

Die Funktion `dbCredentialsPs` ruft das Powershell Skript `getCredentials` auf und liefert die erhaltenen Anmeldedaten für die Microsoft SQL Datenbank zurück.

```python
def dbCredentialsPs():
        p = Popen(["Powershell.exe", "C:\\PythonScripts\\config\\getCredentials.ps1"], 
            stdin=PIPE, stdout=PIPE, stderr=PIPE)
	out, err = p.communicate()
	return out.decode().replace('\r', '').replace('\n', '')
```

Die beiden Funktionen `apiCredentials` und `proxySettings` lesen jeweils aus einer Textdatei einen String aus und geben diesen danach an eine andere Funktion zurück.

```python
def apiCredentials():
    with open('C:\PythonScripts\config\key.txt', 'r') as f:
        return f.readline()

def proxySettings():
    with open('C:\PythonScripts\config\proxy.txt', 'r') as f:
        return f.readline()
```

Die Funktion `getAdminName` gibt den Namen eines Administrators anhand seiner ID zurück.

```python
def getAdminName(admins, adminId):
    for admin in admins:
        if admin['id'] == adminId:
            return admin['name']
```

\pagebreak

Die Funktion `mergeApiRequest` fügt Queries zu einem String zusammen und gibt diesen zurück.

```python
def mergeApiRequest(path, queryString):
    query = 'https:/' + path
    if queryString:
        query += '?' + queryString
    return query
```

**Rückgabewert:**

![Rückgabewert mergeApiRequest()](img/9.PNG)

Die Funktion `getHttpMessage` gibt von einem HTTP-Code die Nachricht zurück (`Not found` anstatt `404`).

```python
def getHttpMessage(code):
    return responses[code]
```

Die Funktion `convertTimestamp` konvertiert einen Zeitstempel von UTC nach CET.

```python
def convertTimestamp(timestamp):
    # convert string to datetime object
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    # specify current timestamp timezone (utc)
    timestamp = timezone('UTC').localize(timestamp)
    # specify new timezone and return formated timestamp
    return timezone('Europe/Berlin').normalize(timestamp).strftime('%d.%m.%y %H:%M:%S')
```

Die Funktion `convertNoneToNull` konvertiert einen `None`-Wert zu einem `Null`-Wert. Dies ist notwendig, da die Meraki API bei leeren Werten `None` zurückliefert, die Datenbank beim Einfügen aber `Null` erwartet.

```python
def convertNoneToNull(value):
    if value is None:
        return ''
    return value
```

\pagebreak

Die Funktion `getCityFromAddress` sucht in `address` nach einer Stadt und gibt diese zurück.

```python
def getCityFromAddress(address):
    # remove new line
    address = re.sub('\\n', ' ', address)
    # get zip code and city
    address = re.findall('[\d]{4,5}\s([\D\-\s]{1,25})', address)
    # create list with values which must be removed
    remove = [' Deutschland', ' Germany', ' Switzerland', '.']
    # iterate through list and remove all found values
    for value in remove:
        address = str(address).replace(value, '')
    return address
```

**Rückgabewert:**

![Rückgabewert getCityFromAddress()](img/10.PNG)

Die Funktion `mergeApiResponseAccess` erstellt aus einer Liste von Werten ein Dictionary. 

```python
def mergeApiResponseAccess(access):
    key = ['ID', 'Name', 'Tags', 'IP', 'MAC', 'Network ID', 'Model', 'Firmware']
    value = [access['serial'], access['name'], access['tags'], access['lanIp'], access['mac'], access['networkId'], access['model'], access['firmware']]   
    return dict(zip(key, value))
...
```

**Rückgabewert:**

![Rückgabewert mergeApiResponseAccess()](img/11.PNG)

Die Funktionen `mergeApiResponseClient` und `mergeApiResponseSwitch` haben dieselbe Funktionalität.

\pagebreak

Die Funktion `mergeApiResponseSwitchSettings` erstellt Dictionaries und gibt eine Liste zurück.

```python
def mergeApiResponseSwitchSettings(port, setting):
    switchSettingsList = list()
    for port, setting in zip(port, setting):
        key = [
            'ID', 'Name', 'Tags', 'Enabled', 'Status', 'PoE enabled', 
            'Type', 'VLAN', 'Voice VLAN', 'Allowed VLAN', 'Speed', 'Duplex'
        ]
        value = [
            port['portId'], port['name'], port['tags'], port['enabled'], 
            setting['status'], port['poeEnabled'], port['type'], port['vlan'], 
            port['voiceVlan'], port['allowedVlans'], setting['speed'], setting['duplex']
        ]
        switchSettingsList.append(dict(zip(key, value)))
    return switchSettingsList
```

### 5.5.3 Exception Handling

Das Skript `exceptions` erzeugt einen Dekorator mit dem auftretende Fehler geloggt werden können.

In der Funktion `createLogger` wird ein neues Objekt vom Typ `logging` erstellt und der Speicherort für das Logfile definiert. Mit dem Parameter `disable_existing_loggers` kann mehr als ein Logger erstellt werden und der Dekorator somit bei verschiedenen Funktionen gleichzeitig genutzt werden.

```python
def createLogger():
    logger = logging.getLogger(__name__)
    logging.config.fileConfig('/path/to/logging.ini', disable_existing_loggers=False)
    return logger
```

In den beiden Funktionen `exceptDeco` und `exceptDecoAsync` werden danach die Dekoratoren definiert. Da es beim `meraki` Modul zwei Arten für die Abfrage gibt (siehe dazu das Kapitel [Benutzung](#benutzung)), müssen dafür zwei unterschiedliche Funktionen verwendet werden.

```python
def exceptDeco(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = createLogger()
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(e, exc_info=True)  
    return wrapper
```

\pagebreak

Zeile | Erklärung
| - | ------- |
| 1 | Als Übergabeparameter erhält der Dekorator die dekorierte Funktion `func`
| 2 | Mit `wraps` wird sichergestellt, dass Name der dekorierten Funktion verwendet wird
| 3 | Funktion welche dekorierte Funktion `func` ausführt, Parameter als `args` und `kwargs`
| 4 | Erzeug ein neues Objekt vom Typ `logging` mithilfe der Funktion `createLogger`
| 5-8 | In `try-except`-Block wird Funktion ausgeführt und auftretende Exceptions behandelt

### 5.5.4 Powershell Skripte für Anmeldedaten

Im Ordner `config` befinden sich die beiden Powershell-Skripte `encrypt` sowie `getCredentials`, welche die Anmeldedaten für die Microsoft SQL Datenbank ver- bzw. entschlüsseln. Beim Verschlüsselt wird eine ID des aktuellen Benutzers erstellt, somit kann diese Datei nur von demselben Benutzer wieder entschlüsselt werden.

# 6. Fehlerbehandlung

Die geworfenen Exceptions werden in die Logdatei `error.log` geschrieben.

## 6.1 Meraki spezifische Fehler

Der API-Key wurde entweder nicht angegeben oder ist nicht korrekt.

```yaml
ERROR Meraki API key needs to be defined
{...}
meraki.exceptions.APIKeyError: Meraki API key needs to be defined
```

```yaml
ERROR organizations, getOrganizations - Unauthorized, {'errors': ['Invalid API key']}
{...}
meraki.exceptions.APIError: 
    organizations, getOrganizations - 401 Unauthorized, {'errors': ['Invalid API key']}
```

\pagebreak

**Folgende Punkte könnten die Ursachen sein:**

- In der Konfigurationsdatei `key.txt` wurde kein API-Key angegeben
- In der Funktion `apiCredentials` wurde ein falscher Pfad zu `key.txt` angegeben
- Beim Aufruf einer Funktion über Frontend wurde kein bzw. ein falscher API-Key übergeben

Der Zugriff auf eine Organisation bzw. das Ändern ist mit dem angegebenen API-Key nicht erlaubt.

```yaml
ERROR organizations, getOrganizations - Forbidden
{...}
meraki.exceptions.APIError: organizations, getOrganizations - 403 Forbidden
```

Beim Aufruf einer Funktion werden falsche bzw. zu viele Parameter übergeben.

```yaml
ERROR networks, getNetworkDevices - 404 Not Found, ...
{...}
meraki.exceptions.APIError: 
    networks, getNetworkDevices - 404 Not Found, <!DOCTYPE html PUBLIC ...
```

```yaml
ERROR getNetworkDevices() takes 2 positional arguments but 3 were given
{...}
TypeError: getNetworkDevices() takes 2 positional arguments but 3 were given
```

Der Zugriff auf eine Organisation wurde erst vor Kurzem im Dashboard aktiviert.

```yaml
ERROR organizations, getOrganizationAdmins 
    - 404 Not Found, please wait a minute if the key or org was just newly created.
{...}
meraki.exceptions.APIError: organizations, getOrganizationAdmins 
    - 404 Not Found, please wait a minute if the key or org was just newly created.
```

## 6.2 Datenbank spezifische Fehler

Eines der übergebenen Dictionaries aus `initiate` ist unvollständig bzw. fehlerhaft.

```yaml
ERROR ('42000', "[42000] [Microsoft][ODBC Driver 17 for SQL Server] 
    [SQL Server]Incorrect syntax near ')'. (102) (SQLExecDirectW)")
{...}
```

**Behebung:** Die Einträge der einzelnen Dictionaries müssen auf Vollständigkeit geprüft werden.

# Linksammlung

[https://github.com/meraki](https://github.com/meraki/dashboard-api-python)

[https://gitlab.com/meraki-tools](https://gitlab.com/meraki-tools)

[https://documentation.meraki.com](https://documentation.meraki.com/zGeneral_Administration/Other_Topics/The_Cisco_Meraki_Dashboard_API)

[https://developer.cisco.com/meraki](https://developer.cisco.com/meraki/whats-new/#!overview)

# Begriffserklärung

Begriff | Erklärung
| -- | ----- |
PIP | Paketmanager für Python, ermöglicht Installation weiterer Module
Virtual Environment | erzeugt eine isolierte Umgebung mit vordefinierten Paketversionen
Setter/Getter | Funktion, die einen Wert ausgibt (`get`) bzw. diesen ändert (`set`)
Dekorator | Objekt, welches als Argument eine zu dekorierende Funktion erhält$^1$
DB Context Manager | Genaue Zuweisung bzw. Freigabe von Ressourcen möglich$^2$
Asynchron | Abfragen die parallel abgearbeitet werden, z.B. über `meraki.aio`$^3$
Meraki API Key | Eindeutiger Identifier und geheimer Token für die Authentifizierung
Connectors | Stellen Funktionalitäten vom Backend dem Frontend zur Verfügung

**Anmerkungen:**

<br>

$^1$ wird in diesem Fall dazu verwendet, um Exceptions einer dekorierten Funktion zu loggen

<br>

$^2$ bei Datenbank-Anfragen wird so die Integrität der Datensätze zu jedem Zeitpunk gewährleistet

<br>

$^3$ mehrere Netzwerke einer Organisation können so über `meraki.aio` parallel abgefragt werden

# Python Style Guide

Als Richtlinie für die Skripts wurde PEP8 genutzt: https://www.python.org/dev/peps/pep-0008/

# Verwendung von PIP

Ein Paket in PIP suchen:

```console
(meraki-v1) C:\PythonScripts> pip search meraki
```

Ein Paket in PIP installieren:

```console
(meraki-v1) C:\PythonScripts> pip install meraki
```

Eine spezifische Version eines Pakets in PIP installieren:

```console
(meraki-v1) C:\PythonScripts> pip install meraki==1.0.0b15
```

Liste aller installierten Module ausgeben:

```console
(meraki-v1) C:\PythonScripts> pip list
```

Liste aller installierten Module in eine Textdatei schreiben:

```console
(meraki-v1) C:\PythonScripts> pip freeze > requirements.txt
```

Aktuell installierte Version eines Moduls ausgeben:

```console
(meraki-v1) C:\PythonScripts> pip show meraki
```

Ein Paket in PIP deinstallieren

```console
(meraki-v1) C:\PythonScripts> pip uninstall meraki
```

\pagebreak

# Erklärung der Module

Name | Beschreibung
| - | ----- |
| os | Für die Verwendung von Betriebssystem spezifischen Funktionen
| re | Stellt reguläre Ausdrücke zur Verfügung
| sys | Für die Verwendung von Python Interpreter spezifischen Funktionen
| pyodbc | Treiber für die Interaktion mit einer Microsoft SQL Datenbank
| asyncio | Stellt Funktionalitäten bereit, um parallele Abfragen zu ermöglichen
| http.client | Liefert die Beschreibung eines HTTP Codes zurück
| subprocess | Erzeugt Prozesse, um beispielsweise Powershell Skripts auszuführen
| pytz | Stellt Funktionalitäten bereit, um Zeitzonen umzurechnen
| logging | Stellt Funktionalitäten für das Loggen von Exceptions bereit

# Informationen zu Template

Genutztes Pandoc Template:

https://github.com/Wandmalfarbe/pandoc-latex-template

Erstellung der PDF Datei aus Markdown in Linux:

pandoc documentation.md -o documentation.pdf --from markdown --template eisvogel --listings