#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

import pyodbc

from exceptions import exceptDeco
from functions import dbCredentialsPs


class Database():
    def __init__(self):
        self.conn = pyodbc.connect(dbCredentialsPs())
        self.curs = self.conn.cursor()

    @exceptDeco
    def create(self):
        if not self.curs.tables(table='meraki_access', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'macAddress varchar(255) PRIMARY KEY, '
                'networkId varchar(255), '
                'serialNumber varchar(255), '
                'name varchar(255), '
                'organizationName varchar(255), '
                'networkName varchar(255), '
                'location varchar(255)'
                ')'
                .format(tableName='meraki_access')
            )
        if not self.curs.tables(table='meraki_client', tableType='TABLE').fetchone():  
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'macAddress varchar(255) PRIMARY KEY, '
                'networkId varchar(255), '
                'serialNumber varchar(255), '
                'name varchar(255), '
                'organizationName varchar(255), '
                'networkName varchar(255)'
                ')'
                .format(tableName='meraki_client')
            )  
        if not self.curs.tables(table='meraki_switch', tableType='TABLE').fetchone():  
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'macAddress varchar(255) PRIMARY KEY, '
                'networkId varchar(255), '
                'serialNumber varchar(255), '
                'name varchar(255), '
                'organizationName varchar(255), '
                'networkName varchar(255), '
                'location varchar(255)'
                ')'
                .format(tableName='meraki_switch')
            )  
        if not self.curs.tables(table='meraki_swPort', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'macAddress varchar(255), '
                'portId int, '
                'PRIMARY KEY (macAddress, portId)'
                ')'
                .format(tableName='meraki_swPort')
            )  
        if not self.curs.tables(table='meraki_request', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'id varchar(255), '
                'name varchar(255), '
                'method varchar(255), '
                'request varchar(255), '
                'status varchar(255), '
                'timestamp varchar(255) PRIMARY KEY, '
                'organizationName varchar(255)'
                ')'
                .format(tableName='meraki_request')
            )
        if not self.curs.tables(table='meraki_policies', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'networkId varchar(255), '
                'networkName varchar(255), '
                'groupPolicyId varchar(255), '
                'groupPolicyName varchar(255), '
                'PRIMARY KEY (networkId, groupPolicyId)'
                ')'
                .format(tableName='meraki_policies')
            )
        if not self.curs.tables(table='meraki_portTags', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'organizationName varchar(255), '
                'networkName varchar(255), '
                'tags varchar(255) COLLATE SQL_Latin1_General_CP1_CS_AS, '
                'PRIMARY KEY (organizationName, networkName, tags)'
                ')'
                .format(tableName='meraki_portTags')
            )
        if not self.curs.tables(table='meraki_organization', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'organizationName varchar(255)'
                ')'
                .format(tableName='meraki_organization')
            )
        if not self.curs.tables(table='meraki_admins', tableType='TABLE').fetchone():
            self.curs.execute(
                'CREATE TABLE {tableName} '
                '('
                'name varchar(255), '
                'email varchar(255), '
                'adminId varchar(255), '
                'organizationName varchar(255), '
                'PRIMARY KEY (email, adminId)'
                ')'
                .format(tableName='meraki_admins')
            )
   
    @exceptDeco
    def insert(self, tableName, values):
        if tableName == 'meraki_access' or tableName == 'meraki_switch':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({macAddress}, {networkId}, {serialNumber}, {name}, {organizationName}, {networkName}, {location}) '
                    ') AS SOURCE '
                    'ON TARGET.{macAddress}=SOURCE.{macAddress} '
                    'WHEN MATCHED AND TARGET.{networkId}<>SOURCE.{networkId} OR TARGET.{name}<>SOURCE.{name} OR TARGET.{organizationName}<>SOURCE.{organizationName} OR TARGET.{networkName}<>SOURCE.{networkName} OR TARGET.{location}<>SOURCE.{location} THEN '
                    'UPDATE SET {networkId}=SOURCE.{networkId}, {serialNumber}=SOURCE.{serialNumber}, {name}=SOURCE.{name}, {organizationName}=SOURCE.{organizationName}, {networkName}=SOURCE.{networkName}, {location}=SOURCE.{location} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({macAddress}, {networkId}, {serialNumber}, {name}, {organizationName}, {networkName}, {location}) VALUES (SOURCE.{macAddress}, SOURCE.{networkId}, SOURCE.{serialNumber}, SOURCE.{name}, SOURCE.{organizationName}, SOURCE.{networkName}, SOURCE.{location});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        macAddress='macAddress',
                        networkId='networkId',
                        serialNumber='serialNumber',
                        name='name',
                        organizationName='organizationName',
                        networkName='networkName',
                        location='location'
                    )
                )
            )
        if tableName == 'meraki_client':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({macAddress}, {networkId}, {serialNumber}, {name}, {organizationName}, {networkName}) '
                    ') AS SOURCE '
                    'ON TARGET.{macAddress}=SOURCE.{macAddress} '
                    'WHEN MATCHED AND TARGET.{networkId}<>SOURCE.{networkId} OR TARGET.{name}<>SOURCE.{name} OR TARGET.{organizationName}<>SOURCE.{organizationName} OR TARGET.{networkName}<>SOURCE.{networkName} THEN '
                    'UPDATE SET {networkId}=SOURCE.{networkId}, {serialNumber}=SOURCE.{serialNumber}, {name}=SOURCE.{name}, {organizationName}=SOURCE.{organizationName}, {networkName}=SOURCE.{networkName} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({macAddress}, {networkId}, {serialNumber}, {name}, {organizationName}, {networkName}) VALUES (SOURCE.{macAddress}, SOURCE.{networkId}, SOURCE.{serialNumber}, SOURCE.{name}, SOURCE.{organizationName}, SOURCE.{networkName});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        macAddress='macAddress',
                        networkId='networkId',
                        serialNumber='serialNumber',
                        name='name',
                        organizationName='organizationName',
                        networkName='networkName'
                    )
                )
            )
        if tableName == 'meraki_swPort':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({macAddress}, {portId}) '
                    ') AS SOURCE '
                    'ON TARGET.{macAddress}=SOURCE.{macAddress} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({macAddress}, {portId}) VALUES (SOURCE.{macAddress}, SOURCE.{portId});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        macAddress='macAddress',
                        portId='portId'
                    )
                )
            )
        if tableName == 'meraki_request':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({id}, {name}, {method}, {request}, {status}, {timestamp}, {organizationName}) '
                    ') AS SOURCE '
                    'ON TARGET.{timestamp}=SOURCE.{timestamp} '
                    'WHEN MATCHED AND TARGET.{name}<>SOURCE.{name} OR TARGET.{method}<>SOURCE.{method} OR TARGET.{request}<>SOURCE.{request} OR TARGET.{status}<>SOURCE.{status} OR TARGET.{organizationName}<>SOURCE.{organizationName} THEN '
                    'UPDATE SET {id}=SOURCE.{id}, {name}=SOURCE.{name}, {method}=SOURCE.{method}, {request}=SOURCE.{request}, {status}=SOURCE.{status}, {organizationName}=SOURCE.{organizationName} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({id}, {name}, {method}, {request}, {status}, {timestamp}, {organizationName}) VALUES (SOURCE.{id}, SOURCE.{name}, SOURCE.{method}, SOURCE.{request}, SOURCE.{status}, SOURCE.{timestamp}, SOURCE.{organizationName});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        id='id',
                        name='name',
                        method='method',
                        request='request',
                        status='status',
                        timestamp='timestamp',
                        organizationName='organizationName'
                    )
                )
            )
        if tableName == 'meraki_policies':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({networkId}, {networkName}, {groupPolicyId}, {groupPolicyName}) '
                    ') AS SOURCE '
                    'ON TARGET.{networkId}=SOURCE.{networkId} AND TARGET.{groupPolicyId}=SOURCE.{groupPolicyId} '
                    'WHEN MATCHED AND TARGET.{networkName}<>SOURCE.{networkName} OR TARGET.{groupPolicyName}<>SOURCE.{groupPolicyName} THEN '
                    'UPDATE SET {networkName}=SOURCE.{networkName}, {groupPolicyName}=SOURCE.{groupPolicyName} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({networkId}, {networkName}, {groupPolicyId}, {groupPolicyName}) VALUES (SOURCE.{networkId}, SOURCE.{networkName}, SOURCE.{groupPolicyId}, SOURCE.{groupPolicyName});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        networkId='networkId',
                        networkName='networkName',
                        groupPolicyId='groupPolicyId',
                        groupPolicyName='groupPolicyName'
                    )
                )
            )
        if tableName == 'meraki_portTags':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({organizationName}, {networkName}, {tags}) '
                    ') AS SOURCE '
                    'ON TARGET.{organizationName}=SOURCE.{organizationName} AND TARGET.{networkName}=SOURCE.{networkName} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({organizationName}, {networkName}, {tags}) VALUES (SOURCE.{organizationName}, SOURCE.{networkName}, SOURCE.{tags});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        organizationName='organizationName',
                        networkName='networkName',
                        tags='tags'
                    )
                )
            )
        if tableName == 'meraki_organization':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({organizationName}) '
                    ') AS SOURCE '
                    'ON TARGET.{organizationName}=SOURCE.{organizationName} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({organizationName}) VALUES (SOURCE.{organizationName});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        organizationName='organizationName'
                    )
                )
            )
        if tableName == 'meraki_admins':
            self.curs.execute(
                (
                    'MERGE INTO {tableName} as TARGET '
                    'USING (SELECT * FROM '
                    '(VALUES {value}) '
                    'AS s ({name}, {email}, {adminId}, {organizationName}) '
                    ') AS SOURCE '
                    'ON TARGET.{email}=SOURCE.{email} AND TARGET.{adminId}=SOURCE.{adminId} '
                    'WHEN NOT MATCHED BY SOURCE THEN '
                    'DELETE '
                    'WHEN NOT MATCHED BY TARGET THEN '
                    'INSERT ({name}, {email}, {adminId}, {organizationName}) VALUES (SOURCE.{name}, SOURCE.{email}, SOURCE.{adminId}, SOURCE.{organizationName});'
                    .format(
                        tableName=tableName,
                        value=','.join([str(i) for i in values]),
                        name='name',
                        email='email',
                        adminId='adminId',
                        organizationName='organizationName'
                    )
                )
            )

    @exceptDeco
    def select(self, tableNames, column, condition, value):
        for tableName in tableNames:
            result = self.curs.execute('SELECT {column} FROM {tableName} WHERE {condition} = ?'.format(tableName=tableName, column=column, condition=condition), value).fetchone()
            if result is not None:
                return tableName, result

    @exceptDeco
    def alter(self, tableName, columnName):
        self.curs.execute(
            'ALTER TABLE tableName ADD columnName varchar(255) NOT NULL DEFAULT(1)'
            .format(
                tableName=tableName, columnName=columnName
            )
        )

    def __enter__(self):
        return self
        
    def __exit__(self, exc_type, exc_value, traceback):
        self.conn.commit()
        self.conn.close()