#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import asyncio
import logging.config
from functools import wraps


def createLogger():
    logger = logging.getLogger(__name__)
    logging.config.fileConfig('/path/to/logging.ini', disable_existing_loggers=False)
    return logger

def exceptDeco(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = createLogger()
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(e, exc_info=True)
    return wrapper

def exceptDecoAsync(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        logger = createLogger()
        try:
            return await func(*args, **kwargs)
        except Exception as e:
            logger.error(e, exc_info=True)
    return wrapper