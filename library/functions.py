#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

from os import path
from re import sub, findall
from datetime import datetime
from http.client import responses
from subprocess import Popen, PIPE

from pytz import timezone

from exceptions import exceptDeco


@exceptDeco
def dbCredentials():
    with open('path/to/db.txt', 'r') as f:
        return f.readline()

@exceptDeco
def dbCredentialsPs():
    p = Popen(["powershell.exe", "path/to/script.ps1"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    return out.decode().replace('\r', '').replace('\n', '')

@exceptDeco
def apiCredentials():
    with open('path/to/key.txt', 'r') as f:
        return f.readline()

@exceptDeco
def proxySettings():
    with open('path/to/proxy.txt', 'r') as f:
        return f.readline()

@exceptDeco
def getAdminName(admins, adminId):
    for admin in admins:
        if admin['id'] == adminId:
            return admin['name']

@exceptDeco
def mergeApiRequest(path, queryString):
    query = 'https:/' + path
    if queryString:
        query += '?' + queryString
    return query

@exceptDeco
def getHttpMessage(code):
    return responses[code]

@exceptDeco
def convertTimestamp(timestamp):
    # convert string to datetime object
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    # specify current timestamp timezone (utc)
    timestamp = timezone('UTC').localize(timestamp)
    # specify new timezone and return formated timestamp
    return timezone('Europe/Berlin').normalize(timestamp).strftime('%d.%m.%y %H:%M:%S')

@exceptDeco
def convertNoneToNull(value):
    if value is None:
        return ''
    return value

@exceptDeco
def getCityFromAddress(address):
    # remove new line
    address = sub('\\n', ' ', address)
    # get zip code and city
    address = findall('[\d]{4,5}\s([\D\-\s]{1,25})', address)
    # create list with values which must be removed
    remove = [' Deutschland', ' Germany', ' Switzerland', '.', '[', '\'', ']']
    # iterate through list and remove all found values
    for value in remove:
        address = str(address).replace(value, '')
    return address

@exceptDeco
def mergeApiResponseAccess(access):
    key = ['ID', 'Name', 'Tags', 'IP', 'MAC', 'Network ID', 'Model', 'Firmware']
    value = [access['serial'], access['name'], access['tags'], access['lanIp'], access['mac'], access['networkId'], access['model'], access['firmware']]   
    return dict(zip(key, value))

@exceptDeco
def mergeApiResponseClient(client):
    key = ['ID', 'Name', 'IP', 'MAC', 'Status', 'Connected to']
    value = [client['id'], client['description'], client['ip'], client['mac'], client['status'], client['recentDeviceMac']]
    return dict(zip(key, value))

@exceptDeco
def mergeApiResponseSwitch(switch):
    key = ['ID', 'Name', 'IP', 'MAC', 'Network ID', 'Model', 'Firmware']
    value = [switch['serial'], switch['name'], switch['lanIp'], switch['mac'], switch['networkId'], switch['model'], switch['firmware']]   
    return dict(zip(key, value))

@exceptDeco
def mergeApiResponseSwitchSettings(port, setting):
    switchSettingsList = list()
    for port, setting in zip(port, setting):
        key = ['ID', 'Name', 'Tags', 'Enabled', 'Status', 'PoE enabled', 'Type', 'VLAN', 'Voice VLAN', 'Allowed VLAN', 'Speed', 'Duplex']
        value = [port['portId'], port['name'], port['tags'], port['enabled'], setting['status'], port['poeEnabled'], port['type'], port['vlan'], port['voiceVlan'], port['allowedVlans'], setting['speed'], setting['duplex']]
        switchSettingsList.append(dict(zip(key, value)))
    return switchSettingsList