import json

import meraki


"""Script saves all network and security related events in a given timespan"""

apimeraki = meraki.DashboardAPI(
    api_key='EnterYourSecretApiKeyHere',
    output_log=False,
    print_console=False,
    suppress_logging=True,
    requests_proxy='EnterProxyServerHere'
)

productsList = ['wireless', 'appliance', 'switch']
timespanList = ['2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-07-01T00:00:00', '2020-08-01T00:00:00']

organizations = apimeraki.organizations.getOrganizations()

for organization in organizations:
    print(f'{organization["name"]} - {organization["id"]}')

networkList = apimeraki.organizations.getOrganizationNetworks('EnterOrganizationIdHere')

for network in networkList:
    for timespan in timespanList:
        for products in productsList:
            try:
                netEvents = apimeraki.networks.getNetworkEvents(
                    network['id'],
                    perPage=1000,
                    #total_pages='all',
                    productType=products,
                    startingAfter=timespan
                )
                secEvents = apimeraki.appliance.getNetworkApplianceSecurityEvents(
                    network['id'],
                    perPage=1000,
                    #total_pages='all,
                    productType=products,
                    startingAfter=timespan
                )
            except Exception as e:
                continue

            with open(f'Logs/Company_NetEvents_{network["name"]}.json', 'a') as out:
                json.dump(netEvents, out)
            with open(f'Logs/Company_SecEvents_{network["name"]}.json', 'a') as out:
                json.dump(secEvents, out)
    print(f'Done with {network["name"]}')