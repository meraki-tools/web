#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

from sys import argv

import meraki.api

from exceptions import exceptDeco
from functions import proxySettings


@exceptDeco
def groupPolicy():
    apiKey = argv[1]
    reqTyp = argv[2]
    params = list(argv[3].split(' '))
    apimeraki = meraki.DashboardAPI(
        api_key=apiKey,
        output_log=False,
        print_console=False,
        suppress_logging=True,
        requests_proxy=proxySettings()
    )
    if reqTyp == 'get':
        print(apimeraki.networks.getNetworkClientPolicy(*params))
    if reqTyp == 'set':
        apimeraki.networks.updateNetworkClientPolicy(*params, devicePolicy='Group policy', groupPolicyId=argv[4])


if __name__ == '__main__':
    groupPolicy()