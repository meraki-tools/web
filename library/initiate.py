#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

import asyncio

import meraki.api
import meraki.aio

from functions import (
    apiCredentials,
    proxySettings,
    getAdminName, 
    mergeApiRequest, 
    getHttpMessage,
    convertTimestamp,
    convertNoneToNull,
    getCityFromAddress
)
from database import Database
from exceptions import exceptDeco, exceptDecoAsync


apiOrgDict = dict()
accessDict = dict()
clientDict = dict()
switchDict = dict()
swPortDict = dict()
apiReqDict = dict()
policyDict = dict()
apiTagSet = set()
apiTagDict = dict()
apiAdmDict = dict()

@exceptDeco
def initiateApi():
    apimeraki = meraki.DashboardAPI(
        api_key=apiCredentials(),
        output_log=False,
        print_console=False,
        suppress_logging=True,
        requests_proxy=proxySettings()
    )
    organizations = apimeraki.organizations.getOrganizations()
    for organization in organizations:
        try:
            admins = apimeraki.organizations.getOrganizationAdmins(organization['id'])
            requests = apimeraki.organizations.getOrganizationApiRequests(organization['id'], timespan=60*60*24*3, perPage=1000, total_pages='all')
        except meraki.exceptions.APIError as e:
            print(e)
        for admin in admins:
            apiAdmDict.update({admin['id'] : (admin['name'], admin['email'], admin['id'], organization['name'])})
        for request in requests:
            apiReqDict.update({request['ts'] : (request['adminId'], getAdminName(admins, request['adminId']), request['method'], mergeApiRequest(request['path'], request['queryString']), getHttpMessage(request['responseCode']), convertTimestamp(request['ts']), organization['name'])})

@exceptDecoAsync
async def initiateAio():
    async with meraki.aio.AsyncDashboardAPI(
        api_key=apiCredentials(),
        output_log=False,
        print_console=False,
        suppress_logging=True,
        requests_proxy=proxySettings()
    ) as aiomeraki:
        organizations = await aiomeraki.organizations.getOrganizations()
        organizationTasks = [listOrganizations(aiomeraki, organization) for organization in organizations]
        for task in asyncio.as_completed(organizationTasks):
            await task
        with Database() as db:
            db.create()
            for table, itemDict in zip(('meraki_access', 'meraki_client', 'meraki_switch'), (accessDict, clientDict, switchDict)):
                db.insert(table, itemDict.values())
            db.insert('meraki_swPort', swPortDict.keys())
            db.insert('meraki_request', apiReqDict.values())
            db.insert('meraki_policies', policyDict.keys())
            db.insert('meraki_organization', apiOrgDict.keys())
            db.insert('meraki_portTags', apiTagDict.keys())
            db.insert('meraki_admins', apiAdmDict.values())

@exceptDecoAsync
async def listOrganizations(aiomeraki: meraki.aio.AsyncDashboardAPI, organization):
    apiOrgDict.update({("('"+organization['name']+"')") : ''})
    networks = await aiomeraki.organizations.getOrganizationNetworks(organization['id'])
    networkTasks = [listNetworks(aiomeraki, organization, network) for network in networks]
    for task in asyncio.as_completed(networkTasks):
        await task
    return None

@exceptDecoAsync
async def listNetworks(aiomeraki: meraki.aio.AsyncDashboardAPI, organization, network):
    clients = await aiomeraki.networks.getNetworkClients(network['id'], timespan=60*60*24*3, perPage=1000, total_pages='all')
    devices = await aiomeraki.networks.getNetworkDevices(network['id'])
    policies = await aiomeraki.networks.getNetworkGroupPolicies(network['id'])
    for client in clients:
        clientDict.update({client['mac'] : (client['mac'], network['id'], client['id'], convertNoneToNull(client['description']), organization['name'], network['name'])})
    for device in devices:
        if device['firmware'].split('-')[0] == 'wireless':   
            accessDict.update({device['mac'] : (device['mac'], network['id'], device['serial'], device['name'], organization['name'], network['name'], getCityFromAddress(device['address']))})
        if device['firmware'].split('-')[0] == 'switch':
            switchDict.update({device['mac'] : (device['mac'], network['id'], device['serial'], device['name'], organization['name'], network['name'], getCityFromAddress(device['address']))})
            swPort = await aiomeraki.switch.getDeviceSwitchPorts(device['serial'])
            for port in swPort:
                apiTagSet.update(port['tags'])
                swPortDict.update({(device['mac'], int(port['portId'])) : ''})
    for tags in apiTagSet:
        apiTagDict.update({(organization['name'], network['name'], tags) : ''})
    for policy in policies:
        policyDict.update({(network['id'], network['name'], policy['groupPolicyId'], policy['name']) : ''})
    return None


if __name__ == "__main__":
    initiateApi()
    asyncio.run(initiateAio())