#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

from sys import argv

import meraki.api

from database import Database
from exceptions import exceptDeco
from functions import (
    proxySettings,
    mergeApiResponseAccess,
    mergeApiResponseClient,
    mergeApiResponseSwitch,
    mergeApiResponseSwitchSettings
)


@exceptDeco
def macLookup():
    macAdd = argv[1]
    apiKey = argv[2]
    apimeraki = meraki.DashboardAPI(
        api_key=apiKey,
        output_log=False,
        print_console=False,
        suppress_logging=True,
        requests_proxy=proxySettings()
    )
    with Database() as db:
        queryResult = db.select(('meraki_access', 'meraki_client', 'meraki_switch'), ('networkId, serialNumber'), 'macAddress', macAdd)
    if queryResult[0] == 'meraki_access':
        print(mergeApiResponseAccess(apimeraki.devices.getDevice(queryResult[1][1])))
    if queryResult[0] == 'meraki_client':
        print(mergeApiResponseClient(apimeraki.networks.getNetworkClient(queryResult[1][0], queryResult[1][1])))
    if queryResult[0] == 'meraki_switch':
        print(list((mergeApiResponseSwitch(apimeraki.devices.getDevice(queryResult[1][1])), mergeApiResponseSwitchSettings(apimeraki.switch.getDeviceSwitchPorts(queryResult[1][1]), apimeraki.switch.getDeviceSwitchPortsStatuses(queryResult[1][1])))))


if __name__ == '__main__':
    macLookup()