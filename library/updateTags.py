#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'July 2020'
__license__ = 'GNU GPLv3'
__version__ = '1.0'

import asyncio

import meraki.aio

from functions import (
    apiCredentials,
    proxySettings
)
from database import Database
from exceptions import exceptDecoAsync


apiTagSet = set()
apiTagDict = dict()

@exceptDecoAsync
async def updateTags():
    async with meraki.aio.AsyncDashboardAPI(
        api_key=apiCredentials(),
        output_log=False,
        print_console=False,
        suppress_logging=True,
        requests_proxy=proxySettings()
    ) as aiomeraki:
        organizations = await aiomeraki.organizations.getOrganizations()
        organizationTasks = [listOrganizations(aiomeraki, organization) for organization in organizations]
        for task in asyncio.as_completed(organizationTasks):
            organizationName = await task
        with Database() as db:
            db.create()
            db.insert('meraki_portTags', apiTagDict.keys())

@exceptDecoAsync
async def listOrganizations(aiomeraki: meraki.aio.AsyncDashboardAPI, organization):   
    networks = await aiomeraki.organizations.getOrganizationNetworks(organization['id'])
    networkTasks = [listNetworks(aiomeraki, organization, network) for network in networks]
    for task in asyncio.as_completed(networkTasks):
        result = await task
    return None

@exceptDecoAsync
async def listNetworks(aiomeraki: meraki.aio.AsyncDashboardAPI, organization, network):
    devices = await aiomeraki.networks.getNetworkDevices(network['id'])
    for device in devices:
        if device['firmware'].split('-')[0] == 'switch':
            swPort = await aiomeraki.switch.getDeviceSwitchPorts(device['serial'])
            for port in swPort:
                apiTagSet.update(port['tags'])
    for tags in apiTagSet:
        apiTagDict.update({(organization['name'], network['name'], tags) : ''})
    return None


if __name__ == "__main__":
    asyncio.run(updateTags())